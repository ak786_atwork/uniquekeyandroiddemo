package com.example.uniquekeyandroiddemo;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textview);

        String[] perms = {Manifest.permission.READ_PHONE_STATE};

        int permsRequestCode = 200;
        ActivityCompat.requestPermissions(this,perms,200);

        showID();
//        textView.setText(getIMEI());
//        textView.setText(getMacAddressFromDevicePolicyManager());
        String[] permissions = {Manifest.permission.READ_PHONE_STATE};
      /*  if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)){

        }*/
//        textView.setText(getIMEI());

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(getIMEI());
            }
        });
    }

    private void showID() {
        String id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        textView.setText(id);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private String getMacAddressFromDevicePolicyManager() {
        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        return devicePolicyManager.getWifiMacAddress(this.getComponentName());
    }

    private String getIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        /*
         * getDeviceId() returns the unique device ID.
         * For example,the IMEI for GSM and the MEID or ESN for CDMA phones.
         */
//        String deviceId = "device id = " + telephonyManager.getDeviceId();
        String deviceId = "device id = ";
        deviceId += "     imei =  "+telephonyManager.getImei();
        deviceId += "mac =" + getMacAddress();
        return deviceId;
    }

    private String getMacAddress() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wInfo = wifiManager.getConnectionInfo();
        String macAddress = wInfo.getMacAddress();
        return macAddress;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
